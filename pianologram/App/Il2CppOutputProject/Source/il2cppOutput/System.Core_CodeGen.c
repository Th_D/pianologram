﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000008 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000009 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000000D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Intersect(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::IntersectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000000F TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000010 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000012 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000015 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000016 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000017 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000018 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000019 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001A System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000001B TAccumulate System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
// 0x0000001C System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583 ();
// 0x0000001D System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000001E TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000001F System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000020 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000021 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000022 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000023 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000024 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000025 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000026 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000027 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000028 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000029 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000002A System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000002B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002C System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000002D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000002E System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000002F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000030 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000031 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000032 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000033 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000035 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000036 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x00000037 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x00000038 System.Boolean System.Linq.Enumerable_<TakeIterator>d__25`1::MoveNext()
// 0x00000039 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::<>m__Finally1()
// 0x0000003A TSource System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000003B System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x0000003C System.Object System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x0000003D System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000003E System.Collections.IEnumerator System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000003F System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x00000040 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x00000041 System.Boolean System.Linq.Enumerable_<UnionIterator>d__71`1::MoveNext()
// 0x00000042 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally1()
// 0x00000043 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally2()
// 0x00000044 TSource System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000045 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x00000046 System.Object System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x00000047 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000048 System.Collections.IEnumerator System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000049 System.Void System.Linq.Enumerable_<IntersectIterator>d__74`1::.ctor(System.Int32)
// 0x0000004A System.Void System.Linq.Enumerable_<IntersectIterator>d__74`1::System.IDisposable.Dispose()
// 0x0000004B System.Boolean System.Linq.Enumerable_<IntersectIterator>d__74`1::MoveNext()
// 0x0000004C System.Void System.Linq.Enumerable_<IntersectIterator>d__74`1::<>m__Finally1()
// 0x0000004D TSource System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000004E System.Void System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.IEnumerator.Reset()
// 0x0000004F System.Object System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.IEnumerator.get_Current()
// 0x00000050 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000051 System.Collections.IEnumerator System.Linq.Enumerable_<IntersectIterator>d__74`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000052 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x00000053 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x00000054 System.Boolean System.Linq.Set`1::Remove(TElement)
// 0x00000055 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x00000056 System.Void System.Linq.Set`1::Resize()
// 0x00000057 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x00000058 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000059 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000005A System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005B System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x0000005C System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x0000005D System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x0000005E System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x0000005F TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000060 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000061 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000062 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000063 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000064 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000065 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000066 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000067 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000068 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000069 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x0000006A System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x0000006B System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x0000006C System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000006D TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000006E System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000006F System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000070 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000071 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000072 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000073 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000074 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000075 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000076 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000077 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000078 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000079 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000007A System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000007B System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000007C System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000007D System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000007E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000007F System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000080 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000081 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000082 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000083 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000084 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000085 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000086 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000087 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000088 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000089 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000008A System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[138] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[138] = 
{
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	102,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[40] = 
{
	{ 0x02000004, { 67, 4 } },
	{ 0x02000005, { 71, 9 } },
	{ 0x02000006, { 80, 7 } },
	{ 0x02000007, { 87, 10 } },
	{ 0x02000008, { 97, 1 } },
	{ 0x02000009, { 98, 8 } },
	{ 0x0200000A, { 106, 12 } },
	{ 0x0200000B, { 118, 12 } },
	{ 0x0200000D, { 130, 8 } },
	{ 0x0200000F, { 138, 3 } },
	{ 0x02000010, { 141, 5 } },
	{ 0x02000011, { 146, 7 } },
	{ 0x02000012, { 153, 3 } },
	{ 0x02000013, { 156, 7 } },
	{ 0x02000014, { 163, 4 } },
	{ 0x02000015, { 167, 21 } },
	{ 0x02000017, { 188, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 5 } },
	{ 0x06000007, { 15, 1 } },
	{ 0x06000008, { 16, 2 } },
	{ 0x06000009, { 18, 2 } },
	{ 0x0600000A, { 20, 2 } },
	{ 0x0600000B, { 22, 1 } },
	{ 0x0600000C, { 23, 2 } },
	{ 0x0600000D, { 25, 1 } },
	{ 0x0600000E, { 26, 2 } },
	{ 0x0600000F, { 28, 3 } },
	{ 0x06000010, { 31, 2 } },
	{ 0x06000011, { 33, 4 } },
	{ 0x06000012, { 37, 4 } },
	{ 0x06000013, { 41, 4 } },
	{ 0x06000014, { 45, 3 } },
	{ 0x06000015, { 48, 3 } },
	{ 0x06000016, { 51, 1 } },
	{ 0x06000017, { 52, 3 } },
	{ 0x06000018, { 55, 2 } },
	{ 0x06000019, { 57, 2 } },
	{ 0x0600001A, { 59, 5 } },
	{ 0x0600001B, { 64, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[190] = 
{
	{ (Il2CppRGCTXDataType)2, 39663 },
	{ (Il2CppRGCTXDataType)3, 40154 },
	{ (Il2CppRGCTXDataType)2, 39664 },
	{ (Il2CppRGCTXDataType)2, 39665 },
	{ (Il2CppRGCTXDataType)3, 40155 },
	{ (Il2CppRGCTXDataType)2, 39666 },
	{ (Il2CppRGCTXDataType)2, 39667 },
	{ (Il2CppRGCTXDataType)3, 40156 },
	{ (Il2CppRGCTXDataType)2, 39668 },
	{ (Il2CppRGCTXDataType)3, 40157 },
	{ (Il2CppRGCTXDataType)2, 39669 },
	{ (Il2CppRGCTXDataType)3, 40158 },
	{ (Il2CppRGCTXDataType)3, 40159 },
	{ (Il2CppRGCTXDataType)2, 31390 },
	{ (Il2CppRGCTXDataType)3, 40160 },
	{ (Il2CppRGCTXDataType)3, 40161 },
	{ (Il2CppRGCTXDataType)2, 39670 },
	{ (Il2CppRGCTXDataType)3, 40162 },
	{ (Il2CppRGCTXDataType)2, 39671 },
	{ (Il2CppRGCTXDataType)3, 40163 },
	{ (Il2CppRGCTXDataType)2, 39672 },
	{ (Il2CppRGCTXDataType)3, 40164 },
	{ (Il2CppRGCTXDataType)3, 40165 },
	{ (Il2CppRGCTXDataType)2, 39673 },
	{ (Il2CppRGCTXDataType)3, 40166 },
	{ (Il2CppRGCTXDataType)3, 40167 },
	{ (Il2CppRGCTXDataType)2, 39674 },
	{ (Il2CppRGCTXDataType)3, 40168 },
	{ (Il2CppRGCTXDataType)2, 39675 },
	{ (Il2CppRGCTXDataType)3, 40169 },
	{ (Il2CppRGCTXDataType)3, 40170 },
	{ (Il2CppRGCTXDataType)2, 31421 },
	{ (Il2CppRGCTXDataType)3, 40171 },
	{ (Il2CppRGCTXDataType)2, 39676 },
	{ (Il2CppRGCTXDataType)2, 39677 },
	{ (Il2CppRGCTXDataType)2, 31422 },
	{ (Il2CppRGCTXDataType)2, 39678 },
	{ (Il2CppRGCTXDataType)2, 39679 },
	{ (Il2CppRGCTXDataType)2, 39680 },
	{ (Il2CppRGCTXDataType)2, 31424 },
	{ (Il2CppRGCTXDataType)2, 39681 },
	{ (Il2CppRGCTXDataType)2, 39682 },
	{ (Il2CppRGCTXDataType)2, 39683 },
	{ (Il2CppRGCTXDataType)2, 31426 },
	{ (Il2CppRGCTXDataType)2, 39684 },
	{ (Il2CppRGCTXDataType)2, 31428 },
	{ (Il2CppRGCTXDataType)2, 39685 },
	{ (Il2CppRGCTXDataType)3, 40172 },
	{ (Il2CppRGCTXDataType)2, 39686 },
	{ (Il2CppRGCTXDataType)2, 31431 },
	{ (Il2CppRGCTXDataType)2, 39687 },
	{ (Il2CppRGCTXDataType)2, 31433 },
	{ (Il2CppRGCTXDataType)2, 31435 },
	{ (Il2CppRGCTXDataType)2, 39688 },
	{ (Il2CppRGCTXDataType)3, 40173 },
	{ (Il2CppRGCTXDataType)2, 39689 },
	{ (Il2CppRGCTXDataType)2, 31438 },
	{ (Il2CppRGCTXDataType)2, 39690 },
	{ (Il2CppRGCTXDataType)3, 40174 },
	{ (Il2CppRGCTXDataType)3, 40175 },
	{ (Il2CppRGCTXDataType)2, 39691 },
	{ (Il2CppRGCTXDataType)2, 31442 },
	{ (Il2CppRGCTXDataType)2, 39692 },
	{ (Il2CppRGCTXDataType)2, 31444 },
	{ (Il2CppRGCTXDataType)2, 31445 },
	{ (Il2CppRGCTXDataType)2, 39693 },
	{ (Il2CppRGCTXDataType)3, 40176 },
	{ (Il2CppRGCTXDataType)3, 40177 },
	{ (Il2CppRGCTXDataType)3, 40178 },
	{ (Il2CppRGCTXDataType)2, 31451 },
	{ (Il2CppRGCTXDataType)3, 40179 },
	{ (Il2CppRGCTXDataType)3, 40180 },
	{ (Il2CppRGCTXDataType)2, 31460 },
	{ (Il2CppRGCTXDataType)2, 39694 },
	{ (Il2CppRGCTXDataType)3, 40181 },
	{ (Il2CppRGCTXDataType)3, 40182 },
	{ (Il2CppRGCTXDataType)2, 31462 },
	{ (Il2CppRGCTXDataType)2, 39487 },
	{ (Il2CppRGCTXDataType)3, 40183 },
	{ (Il2CppRGCTXDataType)3, 40184 },
	{ (Il2CppRGCTXDataType)3, 40185 },
	{ (Il2CppRGCTXDataType)2, 31469 },
	{ (Il2CppRGCTXDataType)2, 39695 },
	{ (Il2CppRGCTXDataType)3, 40186 },
	{ (Il2CppRGCTXDataType)3, 40187 },
	{ (Il2CppRGCTXDataType)3, 39415 },
	{ (Il2CppRGCTXDataType)3, 40188 },
	{ (Il2CppRGCTXDataType)3, 40189 },
	{ (Il2CppRGCTXDataType)2, 31478 },
	{ (Il2CppRGCTXDataType)2, 39696 },
	{ (Il2CppRGCTXDataType)3, 40190 },
	{ (Il2CppRGCTXDataType)3, 40191 },
	{ (Il2CppRGCTXDataType)3, 40192 },
	{ (Il2CppRGCTXDataType)3, 40193 },
	{ (Il2CppRGCTXDataType)3, 40194 },
	{ (Il2CppRGCTXDataType)3, 39421 },
	{ (Il2CppRGCTXDataType)3, 40195 },
	{ (Il2CppRGCTXDataType)3, 40196 },
	{ (Il2CppRGCTXDataType)3, 40197 },
	{ (Il2CppRGCTXDataType)2, 31498 },
	{ (Il2CppRGCTXDataType)2, 31493 },
	{ (Il2CppRGCTXDataType)3, 40198 },
	{ (Il2CppRGCTXDataType)2, 31492 },
	{ (Il2CppRGCTXDataType)2, 39697 },
	{ (Il2CppRGCTXDataType)3, 40199 },
	{ (Il2CppRGCTXDataType)3, 40200 },
	{ (Il2CppRGCTXDataType)3, 40201 },
	{ (Il2CppRGCTXDataType)3, 40202 },
	{ (Il2CppRGCTXDataType)2, 39698 },
	{ (Il2CppRGCTXDataType)3, 40203 },
	{ (Il2CppRGCTXDataType)2, 31511 },
	{ (Il2CppRGCTXDataType)2, 31503 },
	{ (Il2CppRGCTXDataType)3, 40204 },
	{ (Il2CppRGCTXDataType)3, 40205 },
	{ (Il2CppRGCTXDataType)2, 31502 },
	{ (Il2CppRGCTXDataType)2, 39699 },
	{ (Il2CppRGCTXDataType)3, 40206 },
	{ (Il2CppRGCTXDataType)3, 40207 },
	{ (Il2CppRGCTXDataType)3, 40208 },
	{ (Il2CppRGCTXDataType)2, 39700 },
	{ (Il2CppRGCTXDataType)3, 40209 },
	{ (Il2CppRGCTXDataType)2, 31524 },
	{ (Il2CppRGCTXDataType)2, 31516 },
	{ (Il2CppRGCTXDataType)3, 40210 },
	{ (Il2CppRGCTXDataType)3, 40211 },
	{ (Il2CppRGCTXDataType)3, 40212 },
	{ (Il2CppRGCTXDataType)2, 31515 },
	{ (Il2CppRGCTXDataType)2, 39701 },
	{ (Il2CppRGCTXDataType)3, 40213 },
	{ (Il2CppRGCTXDataType)3, 40214 },
	{ (Il2CppRGCTXDataType)3, 40215 },
	{ (Il2CppRGCTXDataType)2, 39702 },
	{ (Il2CppRGCTXDataType)2, 39703 },
	{ (Il2CppRGCTXDataType)3, 40216 },
	{ (Il2CppRGCTXDataType)3, 40217 },
	{ (Il2CppRGCTXDataType)2, 31532 },
	{ (Il2CppRGCTXDataType)3, 40218 },
	{ (Il2CppRGCTXDataType)2, 31533 },
	{ (Il2CppRGCTXDataType)2, 39704 },
	{ (Il2CppRGCTXDataType)3, 40219 },
	{ (Il2CppRGCTXDataType)3, 40220 },
	{ (Il2CppRGCTXDataType)2, 39705 },
	{ (Il2CppRGCTXDataType)3, 40221 },
	{ (Il2CppRGCTXDataType)3, 40222 },
	{ (Il2CppRGCTXDataType)3, 40223 },
	{ (Il2CppRGCTXDataType)2, 31548 },
	{ (Il2CppRGCTXDataType)3, 40224 },
	{ (Il2CppRGCTXDataType)2, 31557 },
	{ (Il2CppRGCTXDataType)3, 40225 },
	{ (Il2CppRGCTXDataType)2, 39706 },
	{ (Il2CppRGCTXDataType)2, 39707 },
	{ (Il2CppRGCTXDataType)3, 40226 },
	{ (Il2CppRGCTXDataType)3, 40227 },
	{ (Il2CppRGCTXDataType)3, 40228 },
	{ (Il2CppRGCTXDataType)3, 40229 },
	{ (Il2CppRGCTXDataType)3, 40230 },
	{ (Il2CppRGCTXDataType)3, 40231 },
	{ (Il2CppRGCTXDataType)2, 31573 },
	{ (Il2CppRGCTXDataType)2, 39708 },
	{ (Il2CppRGCTXDataType)3, 40232 },
	{ (Il2CppRGCTXDataType)3, 40233 },
	{ (Il2CppRGCTXDataType)2, 31577 },
	{ (Il2CppRGCTXDataType)3, 40234 },
	{ (Il2CppRGCTXDataType)2, 39709 },
	{ (Il2CppRGCTXDataType)2, 31587 },
	{ (Il2CppRGCTXDataType)2, 31585 },
	{ (Il2CppRGCTXDataType)2, 39710 },
	{ (Il2CppRGCTXDataType)3, 40235 },
	{ (Il2CppRGCTXDataType)2, 39711 },
	{ (Il2CppRGCTXDataType)3, 40236 },
	{ (Il2CppRGCTXDataType)3, 40237 },
	{ (Il2CppRGCTXDataType)3, 40238 },
	{ (Il2CppRGCTXDataType)2, 31591 },
	{ (Il2CppRGCTXDataType)3, 40239 },
	{ (Il2CppRGCTXDataType)3, 40240 },
	{ (Il2CppRGCTXDataType)2, 31594 },
	{ (Il2CppRGCTXDataType)3, 40241 },
	{ (Il2CppRGCTXDataType)1, 39712 },
	{ (Il2CppRGCTXDataType)2, 31593 },
	{ (Il2CppRGCTXDataType)3, 40242 },
	{ (Il2CppRGCTXDataType)1, 31593 },
	{ (Il2CppRGCTXDataType)1, 31591 },
	{ (Il2CppRGCTXDataType)2, 39713 },
	{ (Il2CppRGCTXDataType)2, 31593 },
	{ (Il2CppRGCTXDataType)3, 40243 },
	{ (Il2CppRGCTXDataType)3, 40244 },
	{ (Il2CppRGCTXDataType)3, 40245 },
	{ (Il2CppRGCTXDataType)2, 31592 },
	{ (Il2CppRGCTXDataType)3, 40246 },
	{ (Il2CppRGCTXDataType)2, 31605 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	138,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	40,
	s_rgctxIndices,
	190,
	s_rgctxValues,
	NULL,
};
